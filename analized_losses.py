import pandas as pd
import matplotlib.image as mpimg
from glob import glob
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import os
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.transforms import ToTensor, Lambda, Compose
import torch.optim as optim
import torchvision
import cv2
import torch.nn.functional as F

from sklearn import manifold

seed = 1
torch.manual_seed(seed)
PATH = "C:\\Users\\USER\\Desktop\\Assaf\\autoencoders\\cell_images\\"
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Hyper Parameters.
learning_rate = 1e-3


class Encoder(nn.Module):
    def __init__(self, latent_dims):
        super(Encoder, self).__init__()
        self.linear1 = nn.Linear(2352, 512)
        self.linear2 = nn.Linear(512, 256)
        self.linear3 = nn.Linear(256, latent_dims)

    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        x = F.relu(self.linear1(x))
        x = F.relu(self.linear2(x))
        return self.linear3(x)


class Decoder(nn.Module):
    def __init__(self, latent_dims):
        super(Decoder, self).__init__()
        self.linear1 = nn.Linear(latent_dims, 256)
        self.linear2 = nn.Linear(256, 512)
        self.linear3 = nn.Linear(512, 2352)

    def forward(self, z):
        z = F.relu(self.linear1(z))
        z = F.relu(self.linear2(z))
        z = torch.sigmoid(self.linear3(z))
        return z.reshape((-1, 28, 28, 3))


class Autoencoder(nn.Module):
    def __init__(self, latent_dims):
        super(Autoencoder, self).__init__()
        self.encoder = Encoder(latent_dims)
        self.decoder = Decoder(latent_dims)

    def forward(self, x):
        z = self.encoder(x)
        return self.decoder(z)


def train(autoencoder, data_x, data_y, latent_dim, check_points, epochs=20, batch=32):
    opt = optim.Adam(autoencoder.parameters())
    total_loss = []
    for epoch in range(1, epochs + 1):
        epoch_loss = []
        for i in range(0, data_y.shape[0], batch):
            x = data_x[i:i+batch].to(device)
            y = data_y[i:i+batch].to(device) # UNUSED.

            opt.zero_grad()
            x_hat = autoencoder(x)
            loss = F.mse_loss(x, x_hat, reduction='sum')
            loss.backward()
            opt.step()
            epoch_loss.append(loss.item() / batch)
        epoch_loss_mean = np.mean(epoch_loss)
        print('epoch - ', epoch, 'avg_loss - ', epoch_loss_mean)
        total_loss.append(epoch_loss_mean)

        if epoch in check_points:
            save_data(autoencoder, total_loss, latent_dim, batch, epoch)

    return autoencoder, total_loss


def plot_latent_tsne(autoencoder, data_x, data_y, num_batches=200):
    z_list = autoencoder.encoder(torch.from_numpy(data_x).to(device)).to('cpu').detach().numpy()
    z_np = np.array(z_list)
    tsne = manifold.TSNE()
    data_tsne = tsne.fit_transform(z_np)
    # plt.scatter(data_tsne[:, 0], data_tsne[:, 1])
    plt.scatter(data_tsne[:, 0], data_tsne[:, 1], c=data_y, cmap='tab10')
    plt.colorbar()


def read_data(path):
    data = pd.DataFrame([{'path': filepath} for filepath in glob(PATH+path+'/*.png')])
    data['image'] = data.apply(lambda q: cv2.imread(q['path']),axis=1)
    data['image'] = data.apply(lambda q: cv2.resize(q['image'], (28,28)).astype('float32') / 256, axis=1)
    if path == 'Parasitized':
        data['class'] = 1.0
    else:
        data['class'] = 0.0
    return data


def save_data(model, total_loss, latent_dims, batch_size, epochs):
    suffix = f"dims{latent_dims}_batch{batch_size}_epochs{epochs}.pt"
    torch.save(model.state_dict(), os.path.join("models", "model_" + suffix))
    np.save(os.path.join("losses", "loss_" + suffix), np.array(total_loss))


def load_save(latent_dims, batch_size, epoch=1000):
    suffix = f"dims{latent_dims}_batch{batch_size}_epochs{epoch}.pt"
    losses = np.load(os.path.join("losses", "loss_" + suffix + ".npy"))
    model = Autoencoder(latent_dims)
    model.load_state_dict(torch.load(os.path.join("models", "model_" + suffix),map_location=torch.device('cpu')))
    return model.eval(), losses / batch_size


def save_cls(model, total_loss, latent_dims, batch_size, epochs):
    suffix = f"dims{latent_dims}_batch{batch_size}_epochs{epochs}.pt"
    torch.save(model.state_dict(), os.path.join("models", "model_cls_" + suffix))
    np.save(os.path.join("losses", "loss_cls_" + suffix), np.array(total_loss))


def load_cls(latent_dims, hidden_size, batch_size, epoch=1000):
    suffix = f"dims{hidden_size}_batch{batch_size}_epochs{epoch}.pt"
    losses = np.load(os.path.join("losses", "loss_cls_" + suffix + ".npy"))
    model = Classifier(latent_dims, hidden_size)
    model.load_state_dict(torch.load(os.path.join("models", "model_cls_" + suffix),map_location=torch.device('cpu')))
    return model.eval(), losses


class Classifier(nn.Module):
    def __init__(self, latent_dims, hidden_size=256):
        super(Classifier, self).__init__()
        self.linear1 = nn.Linear(latent_dims, hidden_size)
        self.linear2 = nn.Linear(hidden_size, hidden_size)
        self.linear3 = nn.Linear(hidden_size, 1)

    def forward(self, z):
        z = F.relu(self.linear1(z))
        z = F.relu(self.linear2(z))
        z = torch.sigmoid(self.linear3(z))
        return z


def train_classifier(latent_dims, hidden_size, batch_size, data_z, data_y, epochs=500):
    classifier = Classifier(latent_dims, hidden_size=hidden_size).to(device)
    total_accuracy = []
    optimizer = optim.Adam(classifier.parameters(), lr=0.01)

    for epoch in range(1, epochs+1):
        epoch_accuracy = []
        for i in range(0,data_y.shape[0], batch_size):
            x = data_z[i:i + batch_size].to(device)
            y = data_y[i:i + batch_size].to(device)
            optimizer.zero_grad()
            output = classifier(x)

            loss = F.binary_cross_entropy(output.reshape(-1, 1), y.reshape(-1, 1), reduction='sum')
            loss.backward()
            optimizer.step()
            epoch_accuracy.append(loss.item() / batch_size)
        epoch_accuracy = np.mean(epoch_accuracy)
        print('epoch - ', epoch, 'accuracy - ', epoch_accuracy)
        total_accuracy.append(epoch_accuracy)

    save_data(classifier, total_accuracy, hidden_size, batch_size, epochs)
    return classifier, total_accuracy

def make_z(autoencoder, data_x):
    z_list = []
    for x in data_x:
        z = autoencoder.encoder(x.to(device).reshape(1, -1))
        z_list.append(z.to(device).detach().cpu().numpy())
    return torch.Tensor(np.array(z_list))


if __name__ == '__main__':
    # min_loss = 100
    # latent_dims = [32, 64, 128, 256]
    # batch_sizes = [16, 32, 64, 128, 256, 512]
    # for latent_dim in latent_dims:
    #     for batch_size in batch_sizes:
    #         m, losses = load_save(latent_dim, batch_size)
    #         plt.plot(np.arange(1000), losses, label='batch - ' + str(batch_size) + ', dim - ' + str(latent_dim))
    #         if (np.min(losses)) < min_loss:
    #             min_loss = np.min(losses)
    #             l = latent_dim
    #             best_autoencoder = m
    #             b = batch_size
    # plt.title('finding the ultimate loss function')
    # plt.xlabel('epochs')
    # plt.ylabel('mean_loss')
    # plt.legend()
    # plt.show()
    # print('ultimate batch size - ', b, ', ultimate latent_dim - ', l)

    l = 128
    b = 128
    best_hidden_layer = 0
    best_batch_size  = 0
    # best_autoencoder, losses = load_save(l, b, 500)
    # best_autoencoder = best_autoencoder.to(device)

    # df1 = read_data('Parasitized')
    # df2 = read_data('Uninfected')
    # df = pd.concat([df1, df2], ignore_index=True).sample(frac=1).reset_index(drop=True)

    # data_x = torch.Tensor(np.array(df['image'].to_list())).to(device)
    # data_y = torch.Tensor(df['class'].to_numpy()).to(device)

    # checkpoints = [500]
    # best_autoencoder = Autoencoder(l).to(device)  # GPU/CPU
    # best_autoencoder, loss = train(
    #     best_autoencoder, data_x, data_y, l, checkpoints, epochs=500, batch=b
    # )

    # data_z = make_z(best_autoencoder, data_x).to(device)

    batch_sizes = [16, 32, 64, 128, 256, 512]
    hidden_layers = [32, 64, 128, 256]
    epochs = 100
    max_ac = 0
    for batch_size in batch_sizes:
        for hidden_layer in hidden_layers:
            # m, ac = train_classifier(l, hidden_layer, batch_size, data_z, data_y, epochs=epochs)
            m, ac = load_cls(l, hidden_layer, batch_size, 100)
            plt.plot(np.arange(epochs), ac, label='batch - ' + str(batch_size) + ', hidden - ' + str(hidden_layer))
            if max(ac) > max_ac:
                max_ac = max(ac)
                best_model = m
                best_hidden_layer = hidden_layer
                best_batch_size  = batch_size
    print('ultimate batch size - ', best_batch_size, ', ultimate hidden layers - ', best_hidden_layer)
    plt.title('finding the ultimate accuracy')
    plt.xlabel('epochs')
    plt.ylabel('accuracy')
    plt.legend()
    plt.show()

